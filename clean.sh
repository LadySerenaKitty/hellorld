#!/bin/sh

FLIST="CMakeCache.txt Makefile Doxyfile cmake_install.cmake install_manifest.txt *.core *~ compile_commands.json build.ninja .ninja_* "
DLIST="CMakeFiles build .usages_clang .cmake"

FILES="src/hellorld.h"

for t in $DLIST
do
	REMS=`find . -type d -name "$t" \( -not -path "./external/*" \)`
	if [ -n "${REMS}" ]; then
		echo "Removing ${t}..."
		rm -r ${REMS}
	fi
done

for t in $FLIST
do
	REMS=`find . -type f  -name "$t" \( -not -path "./external/*" \)`
	if [ -n "${REMS}" ]; then
		echo "Removing ${t}..."
		rm ${REMS}
	fi
done

for t in ${FILES}
do
	if [ -f $t ]; then
		rm $t
	fi
done

